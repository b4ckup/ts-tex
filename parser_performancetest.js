const builder       = require("./parser_utils")
const path          = require("path")
const fs            = require("fs");
const {performance} = require('perf_hooks');

var stressTestFile = "./test/parser_stress_test.txt" 
var runs           = 500;
builder.buildNodes();
var parser = builder.generateParser();

var test  = fs.readFileSync(stressTestFile).toString();
let total = 0;

for(let i=0; i<runs; i++)
{
  let before = performance.now();
  var [result, diagnostics] = parser.parse(test);
  
  total += performance.now() - before;
}

console.log(`${runs} runs at av. ${total/runs}ms`)
