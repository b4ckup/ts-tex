const fs = require("fs");

var apitext = fs.readFileSync("./src/interfaces/api.ts").toString();
var globaltext = fs.readFileSync("./src/Global.ts").toString();

var start = globaltext.indexOf("//#region api.ts text") + "//#region api.ts text".length;
var end = globaltext.indexOf("//#endregion api.ts text");
if(start < 0 || end < 0)
  throw new Error("Could not find start or end api.ts text region!");
var res = globaltext.substring(0, start) + "\n  static API_TEXT = `" + apitext + "`;\n" + globaltext.substring(end);
fs.writeFileSync("./src/Global.ts", res);