import { SourceFile } from "./SourceFile";
import { RootNode } from "./parser/Nodes";


export interface IMapping 
{
  resLineStart: number;
  resLineEnd: number;
  srcLineStart: number;
  srcLineEnd: number;
}

export class SourceMap
{
  fileMappings: SourceMap[] = [];
  mappings: IMapping[] = [];
  line: number;
  end: number;

  constructor(public file: SourceFile, public parent: SourceMap = null, public start: number = 0)
  {
    this.line = this.start;
  }

  setResult(result: string)
  {
    this.end = !result ? this.start : this.start + result.match(/\n/g).length;
  }
  
  addFileMapping(sf: SourceFile): SourceMap
  {
    let map = new SourceMap(sf, this, this.line);
    this.fileMappings.push(map);
    return map;
  }

  addMapping(result: string, node: RootNode)
  {
    let resultLines = 0;
    if(result)
    {
      let match = result.match(/\n/g);
      resultLines = match ? match.length : 0;
    }
    this.mappings.push({
      resLineStart: this.line,
      resLineEnd: this.line + resultLines,
      srcLineStart: node.range.start.line,
      srcLineEnd: node.range.end.line
    });
    this.line += resultLines;
  }

  resolve(line: number): {file: SourceFile, line: number}
  {
    let fm = this.fileMappings.find(m => m.start <= line && line <= m.end);
    if(fm != null)
      return fm.resolve(line);
    
    let m = this.mappings.find(m => m.resLineStart <= line && line <= m.resLineEnd);
    if(m == null)
      throw new Error(`SourceMapError: Could not map line ${line} of file ${this.file.path}`);

    return {
      file: this.file,
      line: Math.min((line - m.resLineStart) + m.srcLineStart - 1, m.srcLineEnd - 1)
    };
  }
}