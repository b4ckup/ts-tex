export interface IPlatform
{
  nodePathDelimiter: string;
  getUserHome(): string;
}

class Unix implements IPlatform
{
  nodePathDelimiter = ":";
  getUserHome()
  {
    return process.env["HOME"];
  }
}

class Win implements IPlatform
{
  nodePathDelimiter = ";";
  getUserHome()
  {
    return process.env["USERPROFILE"];
  }
}

export function getPlatform(): IPlatform
{
  if(process.platform == "win32")
    return new Win();
  else
    return new Unix();
}