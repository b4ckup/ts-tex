import { TsWrapperFile } from "./TsWrapperFile";
import { Global, Ast } from "./main";
import * as path from 'path';
import * as ts_ast from "ts-morph";

export class TsScopeFile extends TsWrapperFile
{
  imports: ts_ast.ImportDeclaration[] = [];
  scopeClass: ts_ast.ClassDeclaration;

  constructor(ast: Ast)
  {
    super(ast, path.join(ast.moduledir, `${Global.FILENAME_SCOPEBASE}.ts`));
    this.create();
    this.file.addStatements(`
    export var ${Global.GLOBALVARNAME_SCOPE}: ${Global.CLASSNAME_SCOPEBASE};
    `);
    this.scopeClass = this.file.addClass({
      name: Global.CLASSNAME_SCOPEBASE,
      isExported: true,
      extends: Global.ABSTRACTNAME_SCOPE
    });

    this.addImport([Global.ABSTRACTNAME_SCOPE], this.ast.apiFile.path);
    let abstractDefinition = this.ast.apiFile.file.getClass(Global.ABSTRACTNAME_SCOPE);
    abstractDefinition.getMethods().forEach(m => {
      this.scopeClass.addMethod({
        name: m.getName(),
        returnType: m.getReturnType().getText(),
        parameters: m.getParameters().map(p => {
          return {
            name: p.getName(),
            type: p.getType().getText()
          };
        })
      });
    });
  }

  protected create()
  {
    this.file = this.ast.tsAst.createSourceFile(this.path);
  }

  public addScopeProperty(name: string, type: string)
  {
    let prop = this.scopeClass.addProperty({
      name: name,
      type: type,
      scope: ts_ast.Scope.Public
    });
  }

  public removeScopeProperty(name: string)
  {
    let prop = this.scopeClass.getProperty(name);
    if(prop)
      prop.remove();
  }
}
