import { TsFile } from "./TsFile";
import * as ts_ast from "ts-morph";
import * as path from 'path';
import { Ast } from "./Ast";

export class TsWrapperFile extends TsFile
{
  imports: ts_ast.ImportDeclaration[] = [];
  constructor(ast: Ast, filename: string)
  {
    super(ast, filename);
  }

  private getNormalizedImportPath(file: string): string
  {
    let dir = path.dirname(this.path);
    if(path.isAbsolute(file) == false)
    {
      file = path.resolve(dir, file);
    }
    file = "./" + path.relative(dir, path.normalize(file));
    if(file.endsWith(".ts"))
      file = file.substr(0, file.length - 3);
    return file;
  }

  addImport(names: string[], file: string): ts_ast.ImportDeclaration
  {
    file = this.getNormalizedImportPath(file);
    let statement = this.file.addImportDeclaration({
      namedImports: names,
      moduleSpecifier: file 
    });
    this.imports.push(statement);
    return statement;
  }

  clearForgottenImports()
  {
    let f = this.imports.filter(i => i.wasForgotten());
    f.forEach(i => this.imports.splice(this.imports.indexOf(i), 1));
  }

  updateImports(names: string[], file: string)
  {
    if(names.length == 0)
      return;
    file = this.getNormalizedImportPath(file);
    if(names.length == 0)
      return;
    let importStatement = this.imports.find(i => i.getModuleSpecifier().getLiteralValue() == file);
    if(importStatement != null && importStatement.wasForgotten() == true)
    {
      importStatement.remove();
      importStatement = null;
    }

    if(importStatement == null)
      importStatement = this.addImport([], file);
    importStatement.removeNamedImports();
    importStatement.addNamedImports(names);
  }

  removeImport(file: string)
  {
    file = this.getNormalizedImportPath(file);
    let importStatement = this.imports.find(i => i.getModuleSpecifier().getLiteralValue() == file);
    if(importStatement != null)
      importStatement.remove();
  }

}
