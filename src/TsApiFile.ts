import { TsWrapperFile } from "./TsWrapperFile";
import { Ast } from "./Ast";
import * as path from 'path';
import * as fs from "fs";
import { Global } from "./Global";

export class TsApiFile extends TsWrapperFile
{
  static DO_NOT_EDIT_COMMENT = "//THIS FILE DEFINES TS-TEX's API AND SHOULD NOT BE EDITED. IF EDITED YOU CAN REWRITE THIS FILE USING TsApiFile.rewrite().\n//CHANGING ANY OF THIS CONTENT WILL NOT AFFECT THE ACTUAL API THAT IS EXPOSED BY THE RUNTIME AND THEREFORE WILL HAVE NO EFFECT!\n\n";

  constructor(ast: Ast)
  {
    super(ast, path.join(ast.moduledir, "_api.ts"));
    if(fs.existsSync(this.path) == true)
    {
      this.file = this.ast.tsAst.addExistingSourceFile(this.path);
    }
    else
    {
      this.file = this.ast.tsAst.createSourceFile(this.path, TsApiFile.DO_NOT_EDIT_COMMENT+"\n"+Global.API_TEXT);
      this.file.save();
    }
    this.rewrite().then(c => {
    });
  }

  public async rewrite()
  {
    this.file.removeText();
    this.file.insertText(0, TsApiFile.DO_NOT_EDIT_COMMENT+Global.API_TEXT);
    await this.file.save();
  }
}
