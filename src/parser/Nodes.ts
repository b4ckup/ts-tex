import { NodeType, INode, ITextNode, IFnNode, IFnNameNode, IParameterListNode, IParameterNode, IParameterSeperatorNode, IAnnotationNode, IRange, IDiagnostic, ILocation } from "../interfaces/api";

const chalk = require("chalk");
export var location;

export type RootNode = FnNode | TextNode;

export function deepJoin(o)
{
  if(Array.isArray(o) == true)
  {
    return o.map(e => deepJoin(e)).filter(e => e != null).join("");
  }
  return o;
}

export interface ICustomDiagnostic extends IDiagnostic
{
  node?: FnNode;
}

export class ParserError extends Error
{
  constructor(message: string, public diagnostics: ICustomDiagnostic[])
  {
    super(message);
  }
}

export abstract class Node implements INode
{
  static count = 0;
  range: IRange;
  type: NodeType;
  constructor(type: NodeType, loc: IRange = null)
  {
    this.range = loc || location();
    this.type = type;
  }
  
  abstract print(text: string): string;
}

export class CommentNode extends Node
{
  constructor(location)
  {
    super("comment");
  }

  print(text: string): string
  {
    return chalk.black.gray(text.substring(this.range.start.offset, this.range.end.offset));
  }

  getText(fileText: string): string
  {
    return fileText.substring(this.range.start.offset, this.range.end.offset);
  }
}

export class TextNode extends Node implements ITextNode
{
  constructor(start: ILocation, end: ILocation)
  {
    super("text");
    this.range = {
      start: start,
      end: end
    };
  }

  print(text: string): string
  {
    return chalk.black.bgWhite(text.substring(this.range.start.offset, this.range.end.offset));
  }

  getText(fileText: string): string
  {
    return fileText.substring(this.range.start.offset, this.range.end.offset);
  }
}

export class FnNode extends Node implements IFnNode
{
  name: FnNameNode;
  parameters: ParameterListNode;
  complete: boolean;
  constructor(name: FnNameNode, parameters: ParameterListNode, complete: boolean = true)
  {
    super("fn");
    this.name       = name;
    this.parameters = parameters;
    this.complete   = complete;
  }

  getStatementText(filetext: string): string
  {
    let res = filetext.substring(this.range.start.offset + 1, this.range.end.offset);
    return res;
  }

  print(text: string): string
  {
    let res = chalk.yellow(text.substr(this.range.start.offset, 1));
    res += this.name.print(text);
    res += chalk.yellow(text.substr(this.range.start.offset + this.name.name.length+1, 1));
    if(this.parameters != null)
      res += this.parameters.print(text);
    if(this.complete == true)
      res += chalk.yellow(text.substr(this.range.end.offset - 1, 2));
    return res;
  }
}

export class FnNameNode extends Node implements IFnNameNode
{
  name: string;
  constructor(name)
  {
      super("fnname");
      this.name = name;
  }

  print(text: string): string
  {
    return chalk.green(this.name);
  }
}

export class ParameterListNode extends Node implements IParameterListNode
{
  parameters: ParameterNode[];
  constructor(parameters: ParameterNode[])
  {
    super("parameterlist");
    this.parameters = parameters || [];
  }

  print(text: string): string
  {
    let res = "";
    for(let p of this.parameters)
    {
      res += p.print(text);
    }
    return res;
  }  
}

export type ParameterNodeKind = "fn" | "obj" | "arr" | "str" | "expr" | "lambda";

export class ParameterNode extends Node implements IParameterNode
{
  annotation: AnnotationNode;
  constructor(private kind: ParameterNodeKind)
  {
    super("parameter");
  }

  print(text: string)
  {
    let color = null;
    switch(this.kind)
    {
      case "arr": {
        color = "bgRed";
        break;
      }
      case "expr": {
        color = "bgYellow";
        break;
      }
      case "fn": {
        color = "bgBlue";
        break;
      }
      case "lambda": {
        color = "bgCyan";
        break;
      }
      case "obj": {
        color = "bgBlueBright";
        break;
      }
      case "str": {
        color = "bgGreenBright";
        break;
      }
      default: {
        color = "bgMagenta";
        break;
      }
    }
    let res = chalk.black[color](text.substring(this.range.start.offset, this.range.end.offset));
    if(this.annotation != null)
      res += this.annotation.print(text);
    return res;
  }
}

export class ParameterSeperatorNode extends Node implements IParameterSeperatorNode
{
  constructor()
  {
    super("parametersep");
  }

  print(text: string)
  {
    return chalk.white.bgBlack(text.substring(this.range.start.offset, this.range.end.offset));
  }
}

export class AnnotationNode extends Node implements IAnnotationNode
{
  constructor()
  {
    super("typeannotation");
  }

  print(text: string)
  {
    return chalk.white.bgGreen(text.substring(this.range.start.offset, this.range.end.offset));
  }
}
