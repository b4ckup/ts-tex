"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var chalk = require("chalk");
function deepJoin(o) {
    if (Array.isArray(o) == true) {
        return o.map(function (e) { return deepJoin(e); }).filter(function (e) { return e != null; }).join("");
    }
    return o;
}
exports.deepJoin = deepJoin;
var ParserError = /** @class */ (function (_super) {
    __extends(ParserError, _super);
    function ParserError(message, diagnostics) {
        var _this = _super.call(this, message) || this;
        _this.diagnostics = diagnostics;
        return _this;
    }
    return ParserError;
}(Error));
exports.ParserError = ParserError;
var Node = /** @class */ (function () {
    function Node(type, loc) {
        if (loc === void 0) { loc = null; }
        this.range = loc || exports.location();
        this.type = type;
    }
    Node.count = 0;
    return Node;
}());
exports.Node = Node;
var CommentNode = /** @class */ (function (_super) {
    __extends(CommentNode, _super);
    function CommentNode(location) {
        return _super.call(this, "comment") || this;
    }
    CommentNode.prototype.print = function (text) {
        return chalk.black.gray(text.substring(this.range.start.offset, this.range.end.offset));
    };
    CommentNode.prototype.getText = function (fileText) {
        return fileText.substring(this.range.start.offset, this.range.end.offset);
    };
    return CommentNode;
}(Node));
exports.CommentNode = CommentNode;
var TextNode = /** @class */ (function (_super) {
    __extends(TextNode, _super);
    function TextNode(start, end) {
        var _this = _super.call(this, "text") || this;
        _this.range = {
            start: start,
            end: end
        };
        return _this;
    }
    TextNode.prototype.print = function (text) {
        return chalk.black.bgWhite(text.substring(this.range.start.offset, this.range.end.offset));
    };
    TextNode.prototype.getText = function (fileText) {
        return fileText.substring(this.range.start.offset, this.range.end.offset);
    };
    return TextNode;
}(Node));
exports.TextNode = TextNode;
var FnNode = /** @class */ (function (_super) {
    __extends(FnNode, _super);
    function FnNode(name, parameters, complete) {
        if (complete === void 0) { complete = true; }
        var _this = _super.call(this, "fn") || this;
        _this.name = name;
        _this.parameters = parameters;
        _this.complete = complete;
        return _this;
    }
    FnNode.prototype.getStatementText = function (filetext) {
        var res = filetext.substring(this.range.start.offset + 1, this.range.end.offset);
        return res;
    };
    FnNode.prototype.print = function (text) {
        var res = chalk.yellow(text.substr(this.range.start.offset, 1));
        res += this.name.print(text);
        res += chalk.yellow(text.substr(this.range.start.offset + this.name.name.length + 1, 1));
        if (this.parameters != null)
            res += this.parameters.print(text);
        if (this.complete == true)
            res += chalk.yellow(text.substr(this.range.end.offset - 1, 2));
        return res;
    };
    return FnNode;
}(Node));
exports.FnNode = FnNode;
var FnNameNode = /** @class */ (function (_super) {
    __extends(FnNameNode, _super);
    function FnNameNode(name) {
        var _this = _super.call(this, "fnname") || this;
        _this.name = name;
        return _this;
    }
    FnNameNode.prototype.print = function (text) {
        return chalk.green(this.name);
    };
    return FnNameNode;
}(Node));
exports.FnNameNode = FnNameNode;
var ParameterListNode = /** @class */ (function (_super) {
    __extends(ParameterListNode, _super);
    function ParameterListNode(parameters) {
        var _this = _super.call(this, "parameterlist") || this;
        _this.parameters = parameters || [];
        return _this;
    }
    ParameterListNode.prototype.print = function (text) {
        var res = "";
        for (var _i = 0, _a = this.parameters; _i < _a.length; _i++) {
            var p = _a[_i];
            res += p.print(text);
        }
        return res;
    };
    return ParameterListNode;
}(Node));
exports.ParameterListNode = ParameterListNode;
var ParameterNode = /** @class */ (function (_super) {
    __extends(ParameterNode, _super);
    function ParameterNode(kind) {
        var _this = _super.call(this, "parameter") || this;
        _this.kind = kind;
        return _this;
    }
    ParameterNode.prototype.print = function (text) {
        var color = null;
        switch (this.kind) {
            case "arr": {
                color = "bgRed";
                break;
            }
            case "expr": {
                color = "bgYellow";
                break;
            }
            case "fn": {
                color = "bgBlue";
                break;
            }
            case "lambda": {
                color = "bgCyan";
                break;
            }
            case "obj": {
                color = "bgBlueBright";
                break;
            }
            case "str": {
                color = "bgGreenBright";
                break;
            }
            default: {
                color = "bgMagenta";
                break;
            }
        }
        var res = chalk.black[color](text.substring(this.range.start.offset, this.range.end.offset));
        if (this.annotation != null)
            res += this.annotation.print(text);
        return res;
    };
    return ParameterNode;
}(Node));
exports.ParameterNode = ParameterNode;
var ParameterSeperatorNode = /** @class */ (function (_super) {
    __extends(ParameterSeperatorNode, _super);
    function ParameterSeperatorNode() {
        return _super.call(this, "parametersep") || this;
    }
    ParameterSeperatorNode.prototype.print = function (text) {
        return chalk.white.bgBlack(text.substring(this.range.start.offset, this.range.end.offset));
    };
    return ParameterSeperatorNode;
}(Node));
exports.ParameterSeperatorNode = ParameterSeperatorNode;
var AnnotationNode = /** @class */ (function (_super) {
    __extends(AnnotationNode, _super);
    function AnnotationNode() {
        return _super.call(this, "typeannotation") || this;
    }
    AnnotationNode.prototype.print = function (text) {
        return chalk.white.bgGreen(text.substring(this.range.start.offset, this.range.end.offset));
    };
    return AnnotationNode;
}(Node));
exports.AnnotationNode = AnnotationNode;
