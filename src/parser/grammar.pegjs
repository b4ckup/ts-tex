
{
  var nodes         = require("./Nodes");
  nodes.location    = location;
  var deepJoin      = nodes.deepJoin;
  var diagnostics   = [];
  var textStart     = null;
}

start
    = tree:((not_anytext / anytext)*) {
      let results = [];
      for(let elem of tree)
      {
        if(Array.isArray(elem))
        {
          for(let el of elem)
          {
            results.push(el);
          }
        }
        else if(elem != null && (elem.type == "fn" || elem.type == "comment"))
        {
          results.push(elem); //push the fn node
        }
      }

      if(textStart != null && results.length != 0)
      {
        results.push(new nodes.TextNode(textStart.start, location().end))
      }
      return [results, diagnostics];
    }

any          = .
ws           = [ \t\r\n]

comment
  = c:( "%" (&(!"\n") .)* "\n"?) {
    return new nodes.CommentNode(location())
  }

not_anytext 
  = node:(embedded_fncall / comment) {
    if(textStart != null)
    {
      let textNode = new nodes.TextNode(textStart.start, node.range.start)
      textStart = null;
      return [textNode, node];
    }
    else
    {
      return node;
    }
  }

anytext      
  = c:any {
    if(textStart == null)
    {
      textStart = location();
    }
  }

fnname 
  = name:("this." [a-zA-Z_.0-9]*) {
    return new nodes.FnNameNode(deepJoin(name))
  }

embedded_fncall 
  = fn:(_0:"\\" _1:(fnname) _2:(_2_0:"(" _2_1:(_2_1_0:(parameter_list?) _2_1_1:")"? _2_1_2:";"?)?)? ) {
    let name = fn[1]
    let params = null;
    let complete = true;
    complete &= fn[2] != null;

    if(fn[2] != null)
    {
      complete &= fn[2][1] != null;
      if(fn[2][1] != null)
      {
        params = fn[2][1][0];
        complete &= fn[2][1][1] != null;
      }
    }
    if(complete != true)
    {
      let loc = location()
      diagnostics.push({
        level: "error",
        message: "Incomplete node, expecting )",
        location: {
          start: loc.end,
          end: loc.end
        }
      });
    }

    return new nodes.FnNode(fn[1], params, complete);
  }

parameter_list 
  = p:(typed_parameter (parameter_seperator parameter_list?)?) {
    let param = p[0];
    let params = p[1];
    let res = [param];
    if(params != null)
    {
      if(params[1] != null)
        res = res.concat(params[0], params[1].parameters);
    }
    return new nodes.ParameterListNode(res);
  }

typed_parameter
  = param:parameter anno:parameter_type_annotation? {
    param.annotation = anno;
    return param;
  }

parameter 
  = param:(ws* node:(function_declaration / lambda / object / array / str / expression) ws*) {
    return new nodes.ParameterNode(param[1]);
  }
  
parameter_type_annotation 
  = anno:(ws* ":" ws* type_annotation ws*){
    return new nodes.AnnotationNode();
  }
  
type_annotation 
  = (object / array / type_lambda / str / type_symbol) (ws* [|&] ws* type_annotation)?

type_lambda
  = l:("(" parameter_list? ")" ws* "=>" ws* body:(object / type_symbol)) {
  }

lambda_input
  = ("(" parameter_list? ")") / ( expression )

lambda
  = l:(lambda_input ws* "=>" ws* body:(object / expression)) {
    return "lambda";
  }

function_declaration
  = "function" ws* "*"? symbol "(" parameter_list? ")" ws* object {
    return "fn";
  } 
  
object 
  = obj: ("{" (str / object / (&(!"}") .))* "}") {
    return "obj";
  }
    
array 
  = arr: ("[" (str / array /(&(!"]") .))* "]") {
    return "arr"
  }
    
parameter_seperator
  = text:(ws* "," ws*) {
  	return new nodes.ParameterSeperatorNode();
  }

fncall 
  = name:(symbol operator_fncall){
    return name;
  }

type_symbol
  = text:([a-zA-Z._\[\]<>0-9]*) {
    return text.join("");
  }

operator_question
  = "?" ws* expression ws* ":" ws *expression

operator_fncall 
  = "(" parameter_list? ")"

operator_index 
  = "[" expression "]" 

operator_dot
  = "." (expression)

operator_single
  = (operator_fncall / operator_dot / operator_index)

operator_ternary 
  = operator_question

operator_dual
  = r:(("+" / "-" / "/" / "*" / "==" / "!=" / ">" / "<" / "||" / "&&" / "|" / "&" / "%") ws* expression?)

expression_subject 
  = fncall / str / symbol / object / array 

expression_subject_bracket
  = "(" expression? ")"

expression
  = r:(ws* ws* operator_spread? (expression_subject / expression_subject_bracket) operator_single* ws* operator_dual* ws* operator_ternary*) 

operator_spread
  = "..."

symbol 
	= text:([a-zA-Z0-9._\-]+){
    return text.join("");
  }
    
keyword 
  = "function" / "class"

str 
	= str_s / str_d / str_i {
    return "str";
  }

str_s
  = r:( "\'" (['^\n] / ((!unscaped_s_quote &(![\n]) any)* unscaped_s_quote))) {
    return deepJoin(r)
  }

str_d
  = r:( "\"" (["^\n] / ((!unscaped_d_quote &(![\n]) any)* unscaped_d_quote?))) {
    return deepJoin(r)
  }

interpolation
  = r:( "${" ws* (expression ws* "}")? ) {
    if(r[2] == null || r[2][2] == null)
    {
      diagnostics.push({
        message: "Fatal: Incomplete string interpolation expression\nThis node was removed from the ast.",
        level: "fatal",
        location: location()
      })
      throw new nodes.ParserError("Incomplete string interpolation expression", diagnostics);
    }
  }
    
str_i
  = r:(_1:("l" / "tikz")? _2:"\`" (&(!"\`")  (interpolation / "\\\`" / any))* "\`" )//( (!unscaped_i_quote  any)* unscaped_i_quote) ) ) {
    {
      let res = deepJoin(r);
      let id = null;
      let tmp = null;
      if(res.startsWith("l") == true)
      {
        id = "LaTeX";
        tmp = res.substr(1);
      }
      else if(res.startsWith("tikz") == true)
      {
        id = "TikZ";
        tmp = res.substr(4);
      }
      if(tmp != null && (tmp.startsWith("`$\u007B") == true || tmp.startsWith("`\\") == true)) //\u007B is squirly left bracket
      {
        diagnostics.push({
          message: id+" Template string should not start with ${*} or \\ (see https://gitlab.com/b4ckup/vscode-ts-tex/issues/11). You can fix this by just inserting a space...",
          level: "warn",
          location: location()
        });
      }
      return res;
  }

unescaped_endline 
  = last:[^\\] "\n" {
    return last;
  }

unscaped_backslash
  = last:[^\\] "\\" {
    return last;
  }

unscaped_d_quote 
  = last:[^\\] "\"" {
    return last;
  }

unscaped_s_quote 
  = last:[^\\] "'" {
    return last;
  }
  
unscaped_i_quote 
  = last:[^\\] "`" {
    return last;
  }
    