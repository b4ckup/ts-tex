import * as parser from "./parser";
import { RootNode, ICustomDiagnostic } from './Nodes';
import { IRange, ILocation, DiagnosticLevel } from "../interfaces/api";

export enum NodeKind
{
  SourceFile = 1,
  FunctionCall = 2,
  ParameterList = 3,
  Parameter = 4,

}

export interface IParserDiagnostic
{
  message: string;
  level: DiagnosticLevel;
  location: IRange;
}

export class Parser
{
  public parse(text: string): [RootNode[], ICustomDiagnostic[]]
  {
    let [nodes, diagnostics] =  parser.parse(text) as [RootNode[], IParserDiagnostic[]];
    return [nodes, this.mapParserDiagnostics(diagnostics)];
  }

  public mapParserDiagnostics(diagnostics: IParserDiagnostic[]): ICustomDiagnostic[]
  {
    return diagnostics.map(d => {
      return {
        location: {
          start: d.location.start.offset - 1,
          end: d.location.end.offset
        },
        message: d.message,
        level: d.level
      };
    });
  }
}
