import { Ast } from './Ast';
import * as ts_ast from "ts-morph";

export class TsFile
{
  ast: Ast;
  path: string;
  file: ts_ast.SourceFile;

  constructor(ast: Ast, path: string)
  {
    this.ast = ast;
    this.path = path;
  }
  
  public remove()
  {
    this.file.delete();
  }
}

