import { TsFile } from "./TsFile";
import { Ast } from "./Ast";
import * as ts_ast from "ts-morph";
import { Global } from "./main";

export class TsCodeFile extends TsFile
{
  constructor(ast: Ast, path: string)
  {
    super(ast, path);
    this.file = this.ast.tsAst.addExistingSourceFile(path);
  }

  public getExportedInterfaces(): ts_ast.InterfaceDeclaration[]
  {
    return this.file.getInterfaces().filter(i => i.isExported() == true);
  }

  public getModuleClasses(): ts_ast.ClassDeclaration[]
  {
    let res = this.file
      .getClasses()
      .filter(c => {
        try
        {
          return c.getImplements().find(c => c.getType().getSymbol().getName() == Global.INTERFACENAME_MODULE) != null;
        }
        catch(error)
        {
          return false;
        }
      });
    return res;
  }

  public getResolvables(): ts_ast.ClassDeclaration[]
  {
    let res = this.file
    .getClasses()
    .filter(c => {
      try
      {
        return c.getImplements().find(c => c.getType().getSymbol().getName() == Global.INTERFACENAME_RESOLVABLE) != null;
      }
      catch(error)
      {
        return false;
      }
    });
    return res;
  }

  public getCompletionProviders(): ts_ast.ClassDeclaration[]
  {
    let res = this.file
    .getClasses()
    .filter(c => {
      try
      {
        return c.getImplements().find(c => c.getType().getSymbol().getName() == Global.INTERFACENAME_COMPLETIONPROVIDER) != null;
      }
      catch(error)
      {
        return false;
      }
    });
    return res;
  }

  public getExports(): string[]
  {
    return [
      ...this.getExportedInterfaces().map(i => i.getName()),
      ...this.getModuleClasses().map(i => i.getName()),
      ...this.getResolvables().map(i => i.getName())
    ];
  }

  public refresh()
  {
    let res = this.file.refreshFromFileSystemSync();
  }
}
