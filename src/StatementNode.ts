import { FnNode } from "./parser/Nodes";
import { SourceFile } from './SourceFile';
import * as ts_ast from "ts-morph";
import { LogLevel, DiagnosticLevel } from './interfaces/api';

export class StatementNode
{
  statement: ts_ast.ExpressionStatement;
  node: FnNode;
  classDeclaration: ts_ast.ClassDeclaration;
  method: ts_ast.MethodDeclaration;

  private get fnNodeStart(): number{
    return this.node.range.start.offset + 1;
  }
  private get fnNodeEnd(): number{
    return this.node.range.end.offset;
  }
  static cnt = 0;
  constructor(classDeclaration: ts_ast.ClassDeclaration, node: FnNode, private sourceFile: SourceFile)
  {
    StatementNode.cnt+=1;
    this.node             = node;
    this.classDeclaration = classDeclaration;
    this.method           = this.classDeclaration.addMethod({
      name: "__dummy"+StatementNode.cnt
    });
    try 
    {
      this.statement        = this.method.addStatements(writer => {
        writer.setIndentationLevel(0);
        writer.write(node.getStatementText(sourceFile.text));
      })[0] as ts_ast.ExpressionStatement;
    } 
    catch (error) 
    {
      this.statement = null;
      this.sourceFile.ast.logger.log("Error during statement node creation", LogLevel.Error, error);
      this.sourceFile.updateDiagnostics.push({
        message: "Cannot create node, most likely imbalanced brackets",
        level: "error",
        location: {
          start: this.node.range.start.offset,
          end: this.node.range.end.offset
        }
      });
    }
  }

  public remove()
  {
    try
    {
      this.method.remove();
    }
    catch(error)
    {
      let k = 10;
    }
  }

  public mapRangeToTsTexFile(start: number, end: number): {start: number, end: number}
  {
    if(this.statement == null)
      return null;
    let nStart = this.fnNodeStart;
    let nEnd = this.fnNodeEnd;
    
    let offset = start - this.statement.getStart();
    let resStart = nStart + offset;
    if(resStart < nStart)
      resStart = nStart;

    offset = end - this.statement.getStart();
    let resEnd = nStart + offset;
    if(resEnd > nEnd)
      resEnd = nEnd;
    return {
      start: resStart,
      end: resEnd
    };
  }

  public mapOffsetToTsFile(pos: number)
  {
    if(this.statement == null)
      return null;
    let offset = pos - this.node.range.start.offset;
    return this.statement.getStart() + offset - 1;
  }
}
