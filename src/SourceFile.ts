import * as path from "path";
export * from "./parser/Nodes";
import { Parser } from './parser/ParserWrapper';
import { FnNode, RootNode, TextNode, ParserError, ICustomDiagnostic, CommentNode } from './parser/Nodes';
import { Ast } from './Ast';
import { Global } from './Global';
import { TsTexWrapperFile } from "./TsTexWrapperFile";
import { ISourceFile, LogLevel, DiagnosticError } from './interfaces/api';
import { NodeExecutionError, CRLFError } from "./Errors";

export class SourceFile implements ISourceFile
{
  path: string;
  get filename() {
    return path.basename(this.path);
  }
  get wrapperFilePath(){
    let fn = this.filename;
    return path.join(this.ast.moduledir, Global.getTsTexWrapperFileName(fn));
  }
  get relativePath(){
    return path.relative(this.ast.workdir, this.path);
  }
  parser = new Parser();
  nodes: RootNode[] = [];
  ast: Ast;
  text: string;
  tsFile: TsTexWrapperFile;
  parserDiagnostics: ICustomDiagnostic[] = [];
  buildDiagnostics: ICustomDiagnostic[] = [];
  updateDiagnostics: ICustomDiagnostic[] = [];
  ok = true;

  get diagnostics() {
    let res = [
      ...this.parserDiagnostics, 
      ...this.buildDiagnostics,
      ...this.updateDiagnostics
    ];
    return res;
  }

  constructor(ast: Ast, filepath: string, text: string)
  {
    this.ast = ast;
    this.path = path.normalize(filepath);
    this.tsFile = new TsTexWrapperFile(ast, this, this.wrapperFilePath);
    this.update(text);
  }

  ensureNoCRLF()
  {
    if(this.text.indexOf("\r") >= 0)
      throw new CRLFError(this.path);
  }

  update(text: string)
  {
    this.updateDiagnostics = [];
    this.ok   = true;
    this.text = text;
    try 
    {
      [this.nodes, this.parserDiagnostics] = this.parser.parse(text);
      this.tsFile.updateNodes(this.nodes.filter(n => n.type == "fn") as FnNode[]);
    } 
    catch (error) 
    {
      this.ok = false;
      if(error instanceof ParserError)
        [this.nodes, this.parserDiagnostics] = [[], error.diagnostics];
      else
        [this.nodes, this.parserDiagnostics] = [[],[]];
      
      this.tsFile.updateNodes(this.nodes.filter(n => n.type == "fn") as FnNode[]);
      this.ast.logger.log(`Error during parsing of file ${this.path}`, LogLevel.Error, error);  
    }
  }

  remove()
  {
    this.tsFile.remove();
  }

  executeNodeAt(pos: number, ignoreIncomplete = false): {result: string, node: FnNode}
  {
    let node = this.tsFile.getStatementNodeAtTsTexFilePosition(pos);
    if(node == null || node.node.type != "fn")
      throw new Error(`Could not find FnNode at sourcefile ${this.filename} position ${pos}`);
    if(node.node.complete == false)
      return null;

    return {
      result: this.executeFnNode(node.node as FnNode, false),
      node: node.node as FnNode
    };
  }
    
  executeFnNode(n: FnNode, isFullBuild: boolean = true): string
  {
    try   
    {
      if(isFullBuild == false)
        this.buildDiagnostics = this.buildDiagnostics.filter(d => d.node == null || d.node.range.start.offset != n.range.start.offset);

      let result = this.ast.scope._executeNode(n as FnNode, this); 
      return result;
    }
    catch(error)
    {
      let err;
      if(error instanceof NodeExecutionError)
      {
        err = error;
        if(error.innerError instanceof DiagnosticError)
        {
          let d = error.innerError.diagnostic as ICustomDiagnostic;
          d.node = n;
          d.location.start += n.range.start.offset;
          if(d.location.end == null)
          d.location.end = n.range.end.offset;
          else
          d.location.end += n.range.start.offset;
          this.buildDiagnostics.push(d);
          err.innerError = new Error(error.innerError.diagnostic.message);
        }
        else if(isFullBuild == true)
        {
          this.buildDiagnostics.push({
            location: {start: n.range.start.offset, end: n.range.end.offset},
            message: `${error.message}, ${error.innerError != null ? error.innerError.message : ""}`,
            level: "error",
            node: n as FnNode
          });
        }
      }
      else if(error instanceof ParserError)
      {
        this.buildDiagnostics.push({
          location: {start: n.range.start.offset, end: n.range.end.offset},
          message: `ParserError at this node: ${error.message}`,
          level: "error",
          node: n as FnNode
        });
        err = error;
      }
      else
      {
        if(isFullBuild == true) //Push all normal errors only on full build
        {
          this.buildDiagnostics.push({
            location: {start: n.range.start.offset, end: n.range.end.offset},
            message: `Unknown Error at this Node: ${error.message}`,
            level: "error",
            node: n as FnNode
          });
        }
        err = new NodeExecutionError(n as FnNode, "EvalError", "Build Error", error, this);
      }
      throw err;
    }
  }

  emit(): string
  {
    this.buildDiagnostics = [];

    if(this.ok == false)
      throw new ParserError(`Could not parse source file ${this.path}. Fix fatal diagnostics before building!`, this.parserDiagnostics);
    if(this.ast.isBuild == true && this.ast.currentSourceMap != null)
      this.ast.currentSourceMap = this.ast.currentSourceMap.addFileMapping(this);

    let result = this.nodes.map(n => {
      let result: string;
      if(n.type == "fn")
      {
        result = this.executeFnNode(n as FnNode);
      }
      else if(n.type == "text" || n.type == "comment")
      {
        result = (n as (TextNode | CommentNode)).getText(this.text);
      }
      else
      {
        throw new Error("Encountered invalid node type, this should never happen!");
      }

      if(this.ast.isBuild == true && this.ast.currentSourceMap != null)
        this.ast.currentSourceMap.addMapping(result, n);

      return result;
    }).join("");

    if(this.ast.isBuild == true && this.ast.currentSourceMap != null)
    {
      this.ast.currentSourceMap.setResult(result);
      this.ast.currentSourceMap = this.ast.currentSourceMap.parent;
    }
    return result;

  }
}
