import { IResolver } from "./interfaces/api";

interface IComponentRegistry
{
  key: string;
  type: string;
  obj: any;
  singleton: boolean;
}

export class Resolver implements IResolver
{
  private registry: IComponentRegistry[] = [];

  resolve<T>(type: string, key: string = null) : T
  {
    let el = this.registry.find(r => r.type == type && r.key == key);
    if(el == null)
      throw Error(`Could not resolve component '${type}' with key '${key}', registered components: ${this.registry.map(e => e.type+"."+e.key).join(", ")}`);

    let obj: T;
    if(el.singleton == false)
      obj = new el.obj() as T;
    else  
      obj = el.obj as T;
    return obj;
  }

  register(object: any, type: string, key?: string, singleton: boolean = true) 
  {
    this.unregister(object);
    this.registry.push({
      key: key,
      type: type,
      obj: object,
      singleton: singleton
    });
  }

  unregister(object: any)
  {
    let index = this.registry.findIndex(r => r.obj == object);
    if(index >= 0)
      this.registry.splice(index, 1);
  }
}
