import { Ast } from "./Ast";
import { TsCodeFile } from "./TsCodeFile";
import { ICompletionItemProvider, IModule, ILifecycleHookComponent, IResolvable } from "./interfaces/api";
import * as ts_ast from "ts-morph";
import { LifecycleHookError } from "./Errors";
import { InputCompletionprovider as InputCompletionProvider } from "./builtin/CompletionProviders";
import { Global } from "./Global";

export interface INamedRegistryComponent<T>
{
  component: T;
  name: string;
}

export interface IRegistryElement
{
  cf: TsCodeFile;
  completionProviders: INamedRegistryComponent<ICompletionItemProvider>[];
  modules: INamedRegistryComponent<IModule>[];
  resolvables: INamedRegistryComponent<IResolvable>[];
}

export class ComponentRegistry
{
  registry: IRegistryElement[] = [];

  get completionProviders(): INamedRegistryComponent<ICompletionItemProvider>[]
  {
    return [].concat.apply([], this.registry.map(r => r.completionProviders));
  }

  get modules(): INamedRegistryComponent<IModule>[]
  {
    return [].concat.apply([], this.registry.map(m => m.modules));
  }

  get lifeCycleHookComponents(): INamedRegistryComponent<ILifecycleHookComponent>[]
  {
    return  [].concat(this.completionProviders, this.modules);
  }
  
  constructor(private ast: Ast)
  {
    let elem: IRegistryElement = {
      cf: null,
      completionProviders: [
        { 
          name: "InputCompletionProvider",
          component: new InputCompletionProvider(this.ast)
        }
      ],
      resolvables: [],
      modules: []
    };
    this.registry.push(elem);
  }

  public runPrebuildHooks()
  {
    this.lifeCycleHookComponents.forEach(c => {
      if(c.component._prebuild)
        c.component._prebuild();
    });
  }

  private getConstructor(cd: ts_ast.ClassDeclaration): [any, string]
  {
    let code = cd.getText();
    let name = cd.getName();
    let js = ts_ast.ts.transpile(code, this.ast.compilerOptions);
    let ctor = eval(js);
    return [ctor, name];
  }

  private instantiateClass<T extends ILifecycleHookComponent>(cd: ts_ast.ClassDeclaration): [T, string] 
  {
    let [ctor, name] = this.getConstructor(cd);
    
    if(name.startsWith("_") == true)
      throw new Error("Illegal Component Name, must not start with an underscore!")
    if(Global.FORBIDDEN_GLOBAL_NAMES.indexOf(name) >= 0)
      throw new Error(`Illegal Component Name, the following names are in use by ts-tex internals: ${JSON.stringify(Global.FORBIDDEN_GLOBAL_NAMES)}!`);

    let m = new ctor() as ILifecycleHookComponent;
    try 
    {
      m._resolver = this.ast.resolver;
      if(m._init != null)
        m._init();
    }
    catch (error) 
    {
      throw new LifecycleHookError("init", name, error);  
    }
    return [m as T, name];
  }

  private toCamelCase(str: string): string
  {
    return str[0].toLowerCase() + str.substr(1);
  }

  public getComponentName(obj: any)
  {
  }

  public addComponents(cf: TsCodeFile)
  {
    let element: IRegistryElement = {
      cf: cf,
      completionProviders: [],
      modules: [],
      resolvables: []
    };
    let exports = cf.getExports();
    this.ast.sourceFiles.forEach(s => {
      s.tsFile.clearForgottenImports();
      s.tsFile.updateImports(exports, cf.path);
    });
    this.ast.scopeFile.clearForgottenImports();
    this.ast.scopeFile.addImport(exports, cf.path);
    cf.getModuleClasses().forEach(m => {
      let [res, name] = this.instantiateClass<IModule>(m);
      let camelName = this.toCamelCase(name);
      if(Global.FORBIDDEN_SCOPE_NAMES.indexOf(camelName) >= 0)
        throw new Error(`Could not add module ${name} to scope, illegal module name. The following module names must not be used: ${Global.FORBIDDEN_SCOPE_NAMES}`);

      this.ast.scopeFile.addScopeProperty(camelName, name);
      this.ast.scope._asAny[camelName] = res;
      if(res._resolvable != null && res._resolvable == true)
        this.ast.resolver.register(res, name);
      element.modules.push({
        component: res,
        name: name
      });
    });
    cf.getResolvables().forEach(m => {
      let [ctor, name] = this.getConstructor(m);
      this.ast.resolver.register(ctor, name, null, false);
      element.resolvables.push({
        component: ctor,
        name: name
      });
    });
    cf.getCompletionProviders().forEach(m => {
      let [provider, name] = this.instantiateClass<ICompletionItemProvider>(m);
      if(provider._resolvable != null && provider._resolvable == true)
        this.ast.resolver.register(provider, name);
      element.completionProviders.push({
        component: provider,
        name: name
      });
    });
    this.registry.push(element);
  }

  public removeComponents(cf: TsCodeFile)
  {
    let element = this.registry.find(e => e.cf == cf);
    if(element == null)
      return;
      
    this.ast.sourceFiles.forEach(s => {
      s.tsFile.removeImport(cf.path);
    });
    this.ast.scopeFile.removeImport(cf.path);
    element.modules.forEach(m => {
      this.ast.scopeFile.removeScopeProperty(m.name);
      this.ast.scope._asAny[m.name] = null;
      this.ast.resolver.unregister(m.component);
      try 
      {
        if(m.component._finalize != null)
          m.component._finalize();
      } 
      catch (error) 
      {
        throw new LifecycleHookError("finalize", m.name, error);  
      }
    });
    element.resolvables.forEach(c => {
      this.ast.resolver.unregister(c.component);
    });
    element.completionProviders.forEach(m => {
      this.ast.resolver.unregister(m.component);
      try 
      {
        if(m.component._finalize != null)
          m.component._finalize();
      } 
      catch (error) 
      {
        throw new LifecycleHookError("finalize", m.name, error);  
      }
    });
    this.registry.splice(this.registry.indexOf(element), 1);
  }
}
