import { FnNode, SourceFile } from "./main";

export type NodeExecutionErrorKind = "IncompleteNode" | "EvalError" ;
export class NodeExecutionError extends Error
{
  constructor(public node: FnNode, public kind: NodeExecutionErrorKind, message: string = null, public innerError: Error = null, public file: SourceFile = null)
  {
    super(message);
  }

  public toString(short: boolean = false): string
  {
    if(short == true)
      return `NodeExecutionError: ${this.message != null ? this.message + " , " : ""}${this.innerError != null ? this.innerError.message : "None"}`;
    else
      return `ts-tex Build Error: ${this.message}\nat node (${this.file.path}:${this.node.range.start.line}:${this.node.range.start.column})\nInnerError: ${this.innerError != null ? this.innerError.stack : "No inner error"}`;
  }
}

export type LifecycleHookErrorKind = "init"|"prebuild"|"postbuild"|"finalize";

export class LifecycleHookError
{
  constructor(public kind: LifecycleHookErrorKind, public componentname: string, public innerError: Error)
  {
  }

  public toString(): string
  {
    return `Component ${this.componentname} threw an error on lifecycle hook ${this.kind}: ${this.innerError.stack}`;
  }
}

export class CRLFError extends Error
{
  constructor(public file: string, message: string = null)
  {
    super(message);
  }

  public toString(): string
  {
    return `ts-tex detected file ${this.file} with CRLF line-endings. This is currently not supported and languageservices might behave off. Please convert the file to linux fileendings (LF)`;
  }
}
