import * as ts_ast from "ts-morph";
import { FnNode, ParserError } from './parser/Nodes';
import { Ast } from "./Ast";
import { NodeExecutionError } from "./Errors";
import * as path from 'path';
import { SourceFile } from "./SourceFile";
import { ScopeAbstract, DiagnosticError as _DiagnosticError } from './interfaces/api';

const DiagnosticError = _DiagnosticError;
const l = String.raw;

export class Scope implements ScopeAbstract
{
  private _config = {};
  private _inputStack = [];
  private _currentFnNode: FnNode = null;
  private _currentNodeSourceFile: SourceFile;
  private _state: "idle"|"building";

  get workdir(): string
  {
    return this._ast.workdir;
  }

  get state(): "idle"|"building"
  {
    return this._state;
  }
  
  constructor(private _ast: Ast)
  {
    this._ast.resolver.register(this, "Scope");
    (global as any).SCOPE = this;
    (global as any).DiagnosticError = DiagnosticError;
    (global as any).RESOLVER = this._ast.resolver;
  }

  //Exposed builtin properties
  input(name: string)
  {
    if(path.isAbsolute(name) == false)
      name = path.resolve(this._ast.workdir, name);
    name = path.normalize(name);
    try 
    {
  
      if(this._inputStack.indexOf(name) >= 0)
        throw new Error(`Input recursion detected: ${this._inputStack.map(s => "*"+path.relative(this._ast.workdir, s)+"*").join(" -> ")} -> ${path.relative(this._ast.workdir, name)}`);
      this._inputStack.push(name);
      
      let sf = this._ast.getSourceFile(name);
      if(sf == null)
        throw new DiagnosticError(`Could not find SourceFile ${name} in ast!`, "error");
  
      let res = sf.emit();
      return res;
    } 
    catch(error)
    {
      this._inputStack = [];
      throw error;
    }
    finally
    {
      let index = this._inputStack.indexOf(name);
      if(index >= 0)
        this._inputStack.splice(index, 1);
    }
  }
  
  configure(key: string, value: any)
  {
    this._config[key] = value;
    return "";
  }

  config<T>(key: string): T
  {
    return this._config[key] as T;
  }

  fn(fn: () => any)
  {
    return this._equalizeLineRangeString(this._convertToString(fn.bind(this)()), fn.toString());
  }

  eval(code: string)
  {
    return this._equalizeLineRangeString(this._convertToString(eval(code)), code);
  }

  str(...o: any[]): string
  {
    return this._convertToString(o);
  }

  //Non exposed private properties
  get _asAny()
  {
    return this as any;
  }

  _prepareForBuild()
  {
    this._state = "building";
    this._inputStack = [
      this._ast.rootFile
    ];
  }

  _setIdle()
  {
    this._state = "idle";
  }

  _executeNode(node: FnNode, file: SourceFile)
  {
    if(node.complete == false)
    throw new NodeExecutionError(node, "IncompleteNode", "This node is not syntactically correct", null, file);
    try
    {
      this._currentFnNode         = node;
      this._currentNodeSourceFile = file;
      let result                  = eval(ts_ast.ts.transpile(node.getStatementText(file.text), this._ast.compilerOptions));
      let str: string             = this._convertToString(result);
      return str
    }
    catch(error)
    {
      if(error instanceof NodeExecutionError || error instanceof ParserError)
        throw error;
      else 
        throw new NodeExecutionError(node, "EvalError", "Error during node evaluation", error, file);
    }
    finally
    {
      this._currentFnNode         = null;
      this._currentNodeSourceFile = null;
    }
  }

  _equalizeLineRangeString(stringresult: string, stringsource: string)
  {
    let lines = (stringsource.match(/\n/g) || []).length;
    let resLines = (stringresult.match(/\n/g) || []).length;
    if(resLines < lines)
      stringresult += "\n".repeat(lines-resLines);
    return stringresult;
  }

  _equalizeLineRangeNode(stringresult: string, node: FnNode)
  {
    let lines = (stringresult.match(/\n/g) || []).length;
    let nodeLines = node.range.end.line - node.range.start.line;
    for(let i=0; i < nodeLines - lines; i++)
    {
      stringresult += "\n";
    }
    return stringresult;
  }

  _isIterable(obj) 
  {
      // checks for null and undefined
      if (obj == null) {
        return false;
      }
      return typeof obj[Symbol.iterator] === 'function';
  }

  _convertToString(o: any): string
  {
    let type = typeof(o);
    if(!o)
    {
    return "";
    }
    else if(type == "string")
    {
      return o;
    }
    else if(type == "number" || type == "boolean")
    {
      return o.toString();
    }
    else if(type == "function")
    {
      return this._convertToString(o());
    }
    if(Array.isArray(o))
    {
      return o.map(e => this._convertToString(e)).join(" ");
    }
    else if(this._isIterable(o) == true)
    {
      o = Array.from(o);
      return o.map(e => this._convertToString(e)).join(" ");
    }
    else if(type == "object")
    {
      return JSON.stringify(o);
    }
  }
}