import { StatementNode } from "./StatementNode";
import { FnNode, Global, Ast } from "./main";
import { TsWrapperFile } from "./TsWrapperFile";
import { SourceFile } from "./SourceFile";
import * as ts_ast from "ts-morph";
import * as path from 'path';

export class TsTexWrapperFile extends TsWrapperFile
{
  imports: ts_ast.ImportDeclaration[] = [];
  dummyClass: ts_ast.ClassDeclaration;
  statements: StatementNode[] = [];
  scopeImport: ts_ast.ImportDeclaration;
  sourceFile: SourceFile;

  constructor(ast: Ast, sf: SourceFile, filename: string)
  {
    super(ast, filename);
    this.sourceFile = sf;
    this.create();
    this.dummyClass = this.file.addClass({
      name: "Scope",
      isExported: false
    });
    this.dummyClass.setExtends(Global.CLASSNAME_SCOPEBASE);
    this.ast.defaultImports.forEach(i => {
      this.addImport(i.names, i.path);
    });
  }

  protected create()
  {
    this.file = this.ast.tsAst.createSourceFile(this.path);
  }

  updateNodes(nodes: FnNode[])
  {
    this.clearStatements();
    nodes.forEach(n => {
      this.statements.push(new StatementNode(this.dummyClass, n, this.sourceFile));
    });
  }

  protected clearStatements()
  {
    this.statements.forEach(s => {
      s.remove();
    });
    this.statements = [];
  }

  getStatementNodeAtTsFilePosition(pos: number): StatementNode
  {
    return this.statements.find(s => s.statement.getStart() <= pos && pos <= s.statement.getEnd());
  }

  getStatementNodeAtTsTexFilePosition(pos: number): StatementNode
  {
    return this.statements.find(s => s.node.range.start.offset <= pos && pos <= s.node.range.end.offset);
  }
}
