import { Ast } from './Ast';
import * as ts_ast from "ts-morph";
import { RootNode, FnNode, ICustomDiagnostic } from './parser/Nodes';
import { SourceFile } from './SourceFile';
import { LogLevel, IFnNode } from './interfaces/api';
import { NodeExecutionError } from './Errors';

interface INodeResult 
{
  result: string;
  node: IFnNode;
  error?: Error | NodeExecutionError;
}
export class LanguageService
{
  ast: Ast;
  tsLangService: ts_ast.LanguageService;
  constructor(ast: Ast)
  {
    this.ast = ast;
    this.tsLangService = ast.tsAst.getLanguageService();
  }

  private map(filepath: string, position: number): [string, number, FnNode, SourceFile]
  {
    let sf = this.ast.getSourceFile(filepath);
    if(sf != null)
    {
      let node = sf.tsFile.getStatementNodeAtTsTexFilePosition(position);
      if(node == null)
      {
        throw new Error(`Could not find statement node for position ${position} in file ${filepath}`);
      }
      let mapped = node.mapOffsetToTsFile(position);
      if(mapped == null)
        return [null,null,null,null];
      return [sf.wrapperFilePath, mapped, node.node, sf];
    }
    return [null,null,null,null];
  }

  private mapDiagnostics(diagnostics: ts_ast.Diagnostic<ts_ast.ts.Diagnostic>[], sf: SourceFile): ts_ast.ts.Diagnostic[]
  {
    return diagnostics
      .map(d => {
        let start = d.getStart();
        if(start == null)
          return null;
        let node = sf.tsFile.getStatementNodeAtTsFilePosition(start);
        if(node != null)
        {
          let range = node.mapRangeToTsTexFile(start, start+d.getLength());
          if(range == null)
            return null;

          d.compilerObject.start = range.start;
          d.compilerObject.length = range.end - range.start;
          return d.compilerObject;
        }
        else
        {
          this.ast.logger.log(`Error mapping diagnostic: Could not find statement node for position ${start}, message: ${d.getMessageText()}, source ${d.getSource()}`, LogLevel.Warn);
        }
      })
      .filter(d => d != null);
  }

  mapBuildFileDiagnostic(line: number)
  {
    if(this.ast.sourceMap != null)
      return this.ast.sourceMap.resolve(line);
    else
      return null;
  }

  mapCustomDiagnostic(d: ICustomDiagnostic, sf: SourceFile): ts_ast.ts.Diagnostic
  {
    return {
      category: d.level == "warn" ? ts_ast.ts.DiagnosticCategory.Warning : 
                d.level == "error" ? ts_ast.ts.DiagnosticCategory.Error : 
                d.level == "fatal" ? ts_ast.ts.DiagnosticCategory.Error :
                ts_ast.ts.DiagnosticCategory.Suggestion,
      code: null,
      file: this.ast.tsAst.getSourceFile(sf.wrapperFilePath).compilerNode,
      start: d.location.start,
      length: d.location.end - d.location.start,
      messageText: d.message
    };
  }

  getDiagnostics(): ts_ast.ts.Diagnostic[]
  {
    let result = [].concat(this.ast.sourceFiles.map(sf => [
      ...this.mapDiagnostics(sf.tsFile.file.getPreEmitDiagnostics(), sf),
      ...sf.diagnostics.map(d => this.mapCustomDiagnostic(d, sf))
    ]));
    return result;
  }

  getDiagnosticsForFile(file: string): ts_ast.ts.Diagnostic[]
  {
    let sf = this.ast.getSourceFile(file);
    if(sf == null)
      throw new Error(`Could not provide diagnostics for source file ${file}, cannot find it in Ast!`);

    let result = [
      ...this.mapDiagnostics(sf.tsFile.file.getPreEmitDiagnostics(), sf) || [],
      ...sf.diagnostics.map(d => this.mapCustomDiagnostic(d, sf))
    ];
    return result;
  }

  getCompletionsAtPosition(offset: number, file: string, triggerCharacter: ts_ast.ts.CompletionsTriggerCharacter): ts_ast.ts.WithMetadata<ts_ast.ts.CompletionInfo>
  {
    let [mapped_file, mapped_offset, node, sf] = this.map(file, offset);
    if(mapped_file == null)
      throw new Error(`LanguageService: Could not map file ${file} and offset ${offset}`);
    let completions = this.tsLangService.compilerObject.getCompletionsAtPosition(mapped_file, mapped_offset, {
      triggerCharacter: triggerCharacter
    });
    if(completions == null)
      return null;
    completions.entries = completions.entries.filter(c => c.name.startsWith("_") == false);
    this.ast.registry.completionProviders.forEach(c => {
      try
      {
        let res = c.component.provideCompletionItems(triggerCharacter, node, sf);
        if(res != null)
          res.forEach(e => {
            completions.entries.push({
              name: e.name,
              kind: "label" as any,
              insertText: e.insertText,
              hasAction: e.hasAction,
              source: e.source,
              isRecommended: e.isRecommended,
              sortText: e.insertText
            });
          });
      }
      catch(error)
      {
        console.log("custom completions error");
        throw error;
      }
    });
    return completions;
  }

  getSignatureHelpAtPosition(offset: number, file: string, triggerCharacter: ts_ast.ts.SignatureHelpTriggerCharacter | ts_ast.ts.SignatureHelpRetriggerCharacter | undefined): ts_ast.ts.SignatureHelpItems
  {
    let [mapped_file, mapped_offset, node, sf] = this.map(file, offset);
    if(mapped_file == null)
      throw new Error(`LanguageService: Could not map file ${file} and offset ${offset}`);
    let reason = {
      triggerCharacter: triggerCharacter,
      kind: undefined
    };
    if(triggerCharacter == ")")
      reason.kind = "retrigger";
    else if(triggerCharacter == undefined)
      reason.kind = "invoked";
    else
      reason.kind = "trigger";

    return this.tsLangService.compilerObject.getSignatureHelpItems(mapped_file, mapped_offset, {
      triggerReason: reason
    });
  }

  getDefinitionAtPosition(offset: number, file: string): ReadonlyArray<ts_ast.ts.DefinitionInfo>
  {
    let [mapped_file, mapped_offset, node, sf] = this.map(file, offset);
    if(mapped_file == null)
      throw new Error(`LanguageService: Could not map file ${file} and offset ${offset}`);
    return this.tsLangService.compilerObject.getDefinitionAtPosition(mapped_file, mapped_offset);
  }

  getTsTexRootNodes(file: string): RootNode[]
  {
    let sf = this.ast.getSourceFile(file);
    if(sf)
      return sf.nodes;
    return null;
  }

  getTsTexNodeAt(offset: number, file: string): RootNode
  {
    let sf = this.ast.getSourceFile(file);
    if(!sf)
      throw new Error(`Could not find sourcfile ${file}`);
    let node = sf.tsFile.getStatementNodeAtTsTexFilePosition(offset);
    if(node == null)
      throw new Error(`Could not find FnNode at sourcefile ${file} position ${offset}`);
    return node.node;
  }

  getHoverAtPosition(offset: number, file: string, nodeEval = true): ts_ast.ts.QuickInfo & INodeResult
  {
    let [tsFile, tsOffset, node, sf] = this.map(file, offset);
    if(!sf)
      throw new Error(`Could not find sourcfile ${file}`);
    let info = this.tsLangService.compilerObject.getQuickInfoAtPosition(tsFile, tsOffset);

    let result: INodeResult = {
      result: null,
      node: node,
      error: null
    };

    if(nodeEval == true)
    {
      try 
      {
        result.result = this.ast.scope._executeNode(node, sf);
      } 
      catch(error) 
      {
        result.error = error;
      }
    }
    
    return Object.assign(result, info);
  }

  executeNodeAtPosition(offset: number, file: string): { result: string, node: FnNode }
  {
    let sf = this.ast.getSourceFile(file);
    if(!sf)
      throw new Error(`Could not find sourcfile ${file}`);
    
    return sf.executeNodeAt(offset);
  }

}
