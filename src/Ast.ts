import { Project, ScriptTarget, CompilerOptions } from "ts-morph";
import { SourceFile } from "./SourceFile";
import * as fs from "fs";
import * as path from 'path';
import { LanguageService } from './LanguageService';
import { Global } from "./main";
import { Scope } from "./Scope";
import { RootNode } from "./parser/Nodes";
import { TsCodeFile } from "./TsCodeFile";
import { TsScopeFile } from "./TsScopeFile";
import { TsApiFile } from "./TsApiFile";
import { Resolver } from "./Resolver";
import { ComponentRegistry } from "./ComponentRegistry";
import { LifecycleHookError } from "./Errors";
import { getPlatform } from "./misc/Platform";
import { ModuleKind, ModuleResolutionKind } from "typescript";
import { SourceMap } from "./SourceMap";
import { LoggerFactory, Logger } from './builtin/LoggerFactory';
import { ILoggerFactory, ILogger, LogLevel } from './interfaces/api';

export class Ast 
{
  tsAst: Project;
  sourceFiles: SourceFile[]   = [];
  tsCodeFiles: TsCodeFile[]   = [];
  scopeFile: TsScopeFile;
  languageService: LanguageService;
  workdir: string;
  rootFile: string;
  apiFile: TsApiFile;
  resolver: Resolver          = new Resolver();
  scope: Scope                = new Scope(this);
  registry: ComponentRegistry = new ComponentRegistry(this);
  platform                    = getPlatform();
  sourceMap: SourceMap;
  compilerOptions: CompilerOptions = {
    target: ScriptTarget.ES2015,
    lib: [
      "es6"
    ],
    module: ModuleKind.None,
    moduleResolution: ModuleResolutionKind.NodeJs,
    typeRoots: [
      path.join(this.platform.getUserHome(), "AppData", "Roaming", "npm", "node_modules", "@types"),
      //local @types is added in ctor
    ],
    rootDir: null //set in ctor
  };
  isBuild = false;
  currentSourceMap: SourceMap;
  loggerFactory: ILoggerFactory;
  logger: ILogger;

  private _originalNodeEnv: string;

  get moduledir(): string {
    return path.join(this.workdir, Global.DIRNAME_TSTEXMODULES);
  }

  get nodeModuledir(): string {
    return path.join(this.workdir, "node_modules");
  }

  get nodes(): RootNode[] {
    return [].concat(this.sourceFiles.map(c => c.nodes));
  }

  get outFile(): string{
    if(this.rootFile == null)
    {
      return null;
    }
    else 
    {
      let fn = path.basename(this.rootFile);
      let dir = this.rootFile.substr(0, this.rootFile.length - fn.length);
      let ext = path.extname(this.rootFile);
      fn = fn.substr(0, fn.length - ext.length);
      let outpath = dir + ".build." + fn + ext;
      return outpath;
    }
  }

  get defaultImports(){
    return [
      {names: [Global.CLASSNAME_SCOPEBASE, Global.GLOBALVARNAME_SCOPE], path: path.join(this.moduledir, Global.FILENAME_SCOPEBASE)},
      {names: [Global.ABSTRACTNAME_SCOPE, "DiagnosticError", "l", "RESOLVER"], path: path.join(this.apiFile.path)},
    ];
  }

  constructor(workdir: string, loggerFactory: ILoggerFactory = null)
  {
    this.loggerFactory           = loggerFactory != null ? loggerFactory : new LoggerFactory();
    this.workdir                 = path.normalize(workdir);
    this.compilerOptions.typeRoots.push(path.join(this.nodeModuledir, "@types"));
    this.compilerOptions.rootDir = this.workdir;

    this.createDirs();
    this.tsAst           = new Project({
      compilerOptions: this.compilerOptions
    });
    this.apiFile         = new TsApiFile(this);
    this.scopeFile       = new TsScopeFile(this);
    this.languageService = new LanguageService(this);
    this._originalNodeEnv = process.env["NODE_PATH"];
    this.logger          = this.loggerFactory.getLogger("Ast");

    //Register components
    this.resolver.register(this.loggerFactory, "LoggerFactory");

    //Add the workdir/node_modules path to the node module resolution environment variable NODE_PATH
    process.env.NODE_PATH = (this._originalNodeEnv != null ? this._originalNodeEnv : "")  + this.nodeModuledir;
    require("module").Module._initPaths();
  }

  private createDirs()
  {
    if(fs.existsSync(this.moduledir) == false)
      fs.mkdirSync(this.moduledir);
  }

  private runPreBuildHooks()
  {
    this.registry.lifeCycleHookComponents.forEach(m => {
      try 
      {
        if(m.component._prebuild != null)
          m.component._prebuild();
      } 
      catch (error) 
      {
        throw new LifecycleHookError("prebuild", m.name, error);
      }
    });
  }

  public runPostBuildHooks()
  {
    this.registry.lifeCycleHookComponents.forEach(m => {
      try 
      {
        if(m.component._postbuild != null)
          m.component._postbuild();
      } 
      catch (error) 
      {
        throw new LifecycleHookError("postbuild", m.name, error);
      }
    });
  }

  public emit(noFileOutput: boolean = false): void
  {
    try
    {
      this.runPreBuildHooks();
  
      if(this.rootFile == null)
        throw new Error("Cannot build document, rootFile is not set!");
      
      let sf = this.getSourceFile(this.rootFile);
      if(sf == null)
        throw new Error(`Could not find rootFile ${this.rootFile} in the Ast!`);
  
      this.sourceMap        = new SourceMap(sf);
      this.currentSourceMap = this.sourceMap;
      this.isBuild          = true;
      let result            = "% !TEX program\n";
      this.sourceMap.line   = 1;

      this.scope._prepareForBuild();
      result += sf.emit();
      
      if(noFileOutput == false)
        fs.writeFileSync(this.outFile, result);
      this.runPostBuildHooks();
    }
    finally
    {
      this.isBuild          = false;
      this.currentSourceMap = null;
      this.scope._setIdle();
    }
  }

  public addCodeFile(path: string)
  {
    this.logger.log(`Adding codefile ${path}`, LogLevel.Verbose);
    let codeFile = new TsCodeFile(this, path);
    this.tsCodeFiles.push(codeFile);
    this.registry.addComponents(codeFile);
  }

  public refreshCodeFile(filepath: string)
  {
    this.logger.log(`Refreshing codefile ${filepath}`, LogLevel.Verbose);
    let cf = this.getCodeFile(filepath);
    if(cf == null)
      throw new Error(`Cannot refresh code file: Could not find file ${filepath} in Ast!`);
    cf.refresh();
    this.registry.removeComponents(cf);
    this.registry.addComponents(cf);
  }

  public getCodeFile(filepath: string)
  {
    filepath = path.normalize(filepath);
    return this.tsCodeFiles.find(f => f.path == filepath);
  }

  public removeCodeFile(filepath: string)
  {
    this.logger.log(`Removing codefile ${filepath}`, LogLevel.Verbose);
    let codeFile = this.getCodeFile(filepath);
    if(codeFile != null)
    {
      this.tsCodeFiles.splice(this.tsCodeFiles.indexOf(codeFile), 1);
      this.registry.removeComponents(codeFile);
      codeFile.remove();
    }
  }

  public async addSourceFile(path: string): Promise<SourceFile>
  {
    this.logger.log(`Adding sourcefile ${path}`, LogLevel.Verbose);
    return new Promise<SourceFile>((resolve, reject) => {
      fs.readFile(path, (err, data) => {
        if(err != null)
        {
          reject(err);
        }
        else
        {
          try 
          {
            let sf = new SourceFile(this, path, data.toString());
            this.sourceFiles.push(sf);
            resolve(sf);      
            return;
          }
          catch (error) 
          {
            reject(error);
            return;
          }
        }
      });
    });
  }

  public removeSourceFile(path: string)
  {
    this.logger.log(`Removing sourcefile ${path}`, LogLevel.Verbose);
    let sf = this.getSourceFile(path);
    if(sf != null)
    {
      this.sourceFiles.splice(this.sourceFiles.indexOf(sf), 1);
      sf.remove();
    }
  }

  public getSourceFile(filepath: string): SourceFile
  {
    filepath = path.normalize(filepath);
    return this.sourceFiles.find(sf => sf.path == filepath);
  }

  public dispose()
  {
    //Reset NODE_PATH
    process.env.NODE_PATH          = this._originalNodeEnv;
    this.compilerOptions.typeRoots = [];
    require("module").Module._initPaths();
  }
}
