import { ILoggerFactory, ILogger, LogLevel } from '../interfaces/api';

export class Logger implements ILogger
{
  constructor(private name: string, private logLambda: (component: string, message: string, level?: LogLevel, error?: any) => void) 
  {
  }

  log(message: string, level: LogLevel = LogLevel.Info, error: any = null) 
  {
    this.logLambda(this.name, message, level, error);
  }
}

export class LoggerFactory implements ILoggerFactory
{
  private _logLambdas: ((component: string, message: string, level?: LogLevel, error?: any) => any)[] = [];
  log(component: string, message: string, level?: LogLevel, error?: any)
  {
    this._logLambdas.forEach(l => {
      l(component, message, level, error);
    });
  }

  getLogger(componentName: string): ILogger 
  {
    return new Logger(componentName, this.log.bind(this));
  }

  registerLogLambda(lambda: (component: string, message: string, level?: LogLevel, error?: any) => any)
  {
    this._logLambdas.push(lambda);
  }
}

