import { ICompletionItem, ICompletionItemProvider, IResolver, IRootNode, ISourceFile, IFnNode } from "../interfaces/api";
import { Ast } from "../Ast";
import * as path from 'path';

export class InputCompletionprovider implements ICompletionItemProvider
{
  _resolver: IResolver;
  constructor(private ast: Ast)
  {
    
  }

  provideCompletionItems(triggerChar: string, node: IRootNode, file: ISourceFile): ICompletionItem[] 
  {
    if(triggerChar == "\"" && node.type == "fn" && (node as IFnNode).name.name == "this.input")
    {
      return this.ast.sourceFiles
        .filter(s => s != file)
        .map(s => {
          let p = path.relative(this.ast.workdir, s.path).replace("\\", "/");
          return {
            name: p,
            sortText: p
          };
        });
    }
    return null;
  }
}

export class ConfigCompletionprovider implements ICompletionItemProvider
{
  _resolver: IResolver;
  constructor(private ast: Ast)
  {
    
  }

  provideCompletionItems(triggerChar: string, node: IRootNode, file: ISourceFile): ICompletionItem[] 
  {
    if(triggerChar == "\"" || triggerChar == "." && node.type == "fn" && (node as IFnNode).name.name == "this.config")
    {
      //TODO
    }
    return null;
  }
}
