var fs      = require("fs")
var util    = require("./parser_utils")

var nodesFile   = "./src/parser/Nodes.ts";
var grammarFile = "./src/parser/grammar.pegjs";
var testFile    = "./test/parserTest";

var noPrint     = process.argv.indexOf("--no-print") >= 0

util.buildNodes();
var parser      = util.generateParser();
if(process.argv.indexOf("-s") < 0 && noPrint == false)
  parseTestFile();

var timeout = null;
var cnt     = 0;

function parseTestFile()
{
  var test = fs.readFileSync(testFile).toString();
  try 
  {
    console.time();
    var [result, diagnostics] = parser.parse(test);
    console.timeEnd();
    // if(diagnostics.length != 0)
    //   console.log("Diagnostics:\n"+JSON.stringify(diagnostics, null, 3))
    if(noPrint == false)
      util.printResult(result, test);
      
  }
  catch(error)
  {
    console.log("Parsing Error: "+error+"\nStack: "+error.stack);
  }
  finally
  {
    cnt += 1;
    console.log("---------------------------------------------------------------"+cnt);
  }
}

if(process.argv.indexOf("-w") >= 0)
{
  fs.watch(nodesFile, (ev, fn) => {
    if(timeout)
      clearTimeout(timeout);
    timeout = setTimeout(() => {
      util.buildNodes();
      util.generateParser();
      parseTestFile();
    });
  });
  
  fs.watch(grammarFile, (ev, fn) => {
    if(timeout)
      clearTimeout(timeout);
    timeout = setTimeout(() => {
      timeout = null;
      util.generateParser();
      parseTestFile();
    }, 300);
  });
  fs.watch(testFile, (ev, fn) => {
    if(timeout)
      clearTimeout(timeout);
    var timeout = setTimeout(() => {
      timeout = null;
      if(parser != null)
      {
        parseTestFile();
      }
    }, 300);
  });
}
