var peg     = require("pegjs")
var fs      = require("fs")
var chalk   = require("chalk");
var ts_ast  = require("ts-morph");
var path    = require("path");
var ts      = ts_ast.ts;
var importFresh = require("import-fresh");

var nodesFile   = "./src/parser/Nodes.ts";
var grammarFile = "./src/parser/grammar.pegjs";

function buildNodes()
{
  try
  {
    var content = fs.readFileSync(nodesFile).toString();
    var transpiled = ts.transpile(content, {
      allowJs: true,
      module: "commonjs"
    });
    fs.writeFileSync(nodesFile.substr(0, nodesFile.length-2)+"js", transpiled);
  }
  catch(error)
  {
    console.log("TsBuild Error: "+error.message+"\nStack: "+error.stack);
  }
}

module.exports.buildNodes = buildNodes;

function generateParser()
{
  try
  {
    var grammar = fs.readFileSync(grammarFile).toString()
    console.log("Generating parser code...");
    let code = peg.generate(grammar, {
      format: "commonjs",
      trace: false,
      optimize: "speed",
      output: "source"
    });
    fs.writeFileSync(path.join(path.dirname(grammarFile), "parser.js"), code);
    if(fs.existsSync("./dist") == false)
      fs.mkdirSync("./dist");
    if(fs.existsSync("./dist/parser") == false)
      fs.mkdirSync("./dist/parser"); 
    fs.writeFileSync(path.join("./dist/parser", "parser.js"), code);

    let name = require.resolve("./src/parser/parser");
    if(require.cache[name] != null)
      delete require.cache["./src/parser/parser"];
    parser = importFresh("./src/parser/parser");
    return parser;

  }
  catch(error)
  {
    let m = "Build error: "+error;
    if(error.location)
    {
      m+= "\nErrorLocation:\n"
        +grammar.substr(error.location.start.offset-20,20)
        +chalk.red(grammar.substring(error.location.start.offset, error.location.end.offset))
        +grammar.substr(error.location.end.offset, 20)
    }
    console.log(m);
    return null;
  }
}

module.exports.generateParser = generateParser;

function printResult(result, text)
{
  let res = "";
  for(let node of result)
  {
    res += node.print(text);
  }
  console.log(res);
}

module.exports.printResult = printResult;