#! /bin/bash
set -e
npm install
node prebuild.js
node parser_build.js -c -s
tsc